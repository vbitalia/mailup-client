<?php
/**
 * MailUp REST API Client.
 *
 * @package VB\MailUp
 * @copyright 2015 VB Italia Srl
 * @since 1.0.0
 */

namespace VB\MailUp;

use VB\MailUp\Exception as MailUpException;

/**
 * MailUp REST API Client.
 *
 * @package VB\MailUp
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 1.0.0
 * @version 1.0.0
 */
class Client {
  /**
   * Logon Endpoint.
   *
   * @since 1.0.0
   * @access private
   * @var string
   */
  private $logon_endpoint;

  /**
   * Authorization Endpoint.
   *
   * @since 1.0.0
   * @access private
   * @var string
   */
  private $authorization_endpoint;

  /**
   * Token Endpoint.
   *
   * @since 1.0.0
   * @access private
   * @var string
   */
  private $token_endpoint;

  /**
   * Console Endpoint.
   *
   * @since 1.0.0
   * @access private
   * @var string
   */
  private $console_endpoint;

  /**
   * E-mail Statistics Endpoint.
   *
   * @since 1.0.0
   * @access private
   * @var string
   */
  private $mail_statistics_endpoint;

  /**
   * Client ID.
   *
   * @since 1.0.0
   * @access private
   * @var string
   */
  private $client_id;

  /**
   * Client Secret.
   *
   * @since 1.0.0
   * @access private
   * @var string
   */
  private $client_secret;

  /**
   * Callback URI.
   *
   * @since 1.0.0
   * @access private
   * @var string
   */
  private $callback_uri;

  /**
   * Access Token.
   *
   * @since 1.0.0
   * @access private
   * @var string
   */
  private $access_token;

  /**
   * Refresh Token.
   *
   * @since 1.0.0
   * @access private
   * @var string
   */
  private $refresh_token;

  /**
   * Setup object.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $client_id Client ID.
   * @param string $client_secret Client Secret.
   * @param string $callback_uri Callback URI.
   */
  public function __construct($client_id, $client_secret, $callback_uri) {
    $endpoint_domain = 'https://services.mailup.com';
    $this->logon_endpoint = $endpoint_domain.'/Authorization/OAuth/LogOn';
    $this->authorization_endpoint = $endpoint_domain.'/Authorization/OAuth/Authorization';
    $this->token_endpoint = $endpoint_domain.'/Authorization/OAuth/Token';
    $this->console_endpoint = $endpoint_domain.'/API/v1.1/Rest/ConsoleService.svc';
    $this->mail_statistics_endpoint = $endpoint_domain.'/API/v1.1/Rest/MailStatisticsService.svc';

    $this->client_id = $client_id;
    $this->client_secret = $client_secret;
    $this->callback_uri = $callback_uri;
    $this->loadToken();
  }

  /**
   * Retrieve Logon Endpoint.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string Logon Endpoint.
   */
  public function getLogonEndpoint() {
    return $this->logon_endpoint;
  }

  /**
   * Retrieve Authorization Endpoint.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string Authorization Endpoint.
   */
  public function getAuthorizationEndpoint() {
    return $this->authorization_endpoint;
  }

  /**
   * Retrieve Token Endpoint.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string Token Endpoint.
   */
  public function getTokenEndpoint() {
    return $this->token_endpoint;
  }

  /**
   * Retrieve Console Endpoint.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string Console Endpoint.
   */
  public function getConsoleEndpoint() {
    return $this->console_endpoint;
  }

  /**
   * Retrieve E-mail Statistics Endpoint.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string E-mail Statistics Endpoint.
   */
  public function getMailStatisticsEndpoint() {
    return $this->mail_statistics_endpoint;
  }

  /**
   * Retrieve Client ID.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string Client ID.
   */
  public function getClientId() {
    return $this->client_id;
  }

  /**
   * Retrieve Client Secret.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string Client Secret.
   */
  public function getClientSecret() {
    return $this->client_secret;
  }

  /**
   * Retrieve Callback URI.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string Callback URI.
   */
  public function getCallbackUri() {
    return $this->callback_uri;
  }

  /**
   * Retrieve Access Token.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string Access Token.
   */
  public function getAccessToken() {
    return $this->access_token;
  }

  /**
   * Retrieve Refresh Token.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string Refresh Token.
   */
  public function getRefreshToken() {
    return $this->refresh_token;
  }

  /**
   * Set Logon Endpoint.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $logon_endpoint Logon Endpoint.
   */
  public function setLogonEndpoint($logon_endpoint) {
    $this->logon_endpoint = $logon_endpoint;
  }

  /**
   * Set Authorization Endpoint.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $authorization_endpoint Authorization Endpoint.
   */
  public function setAuthorizationEndpoint($authorization_endpoint) {
    $this->authorization_endpoint = $authorization_endpoint;
  }

  /**
   * Set Token Endpoint.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $token_endpoint Token Endpoint.
   */
  public function setTokenEndpoint($token_endpoint) {
    $this->token_endpoint = $token_endpoint;
  }

  /**
   * Set Console Endpoint.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $console_endpoint Console Endpoint.
   */
  public function setConsoleEndpoint($console_endpoint) {
    $this->console_endpoint = $console_endpoint;
  }

  /**
   * Set E-mail Statistics Endpoint.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $mail_statistics_endpoint E-mail Statistics Endpoint.
   */
  public function setMailStatisticsEndpoint($mail_statistics_endpoint) {
    $this->mail_statistics_endpoint = $mail_statistics_endpoint;
  }

  /**
   * Set Client ID.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $client_id Client ID.
   */
  public function setClientId($client_id) {
    $this->client_id = $client_id;
  }

  /**
   * Set Client Secret.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $client_secret Client Secret.
   */
  public function setClientSecret($client_secret) {
    $this->client_secret = $client_secret;
  }

  /**
   * Set Callback URI.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $callback_uri Callback URI.
   */
  public function setCallbackUri($callback_uri) {
    $this->callback_uri = $callback_uri;
  }

  /**
   * Set Access Token.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $access_token Access Token.
   */
  public function setAccessToken($access_token) {
    $this->access_token = $access_token;
  }

  /**
   * Set Refresh Token.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $refresh_token Refresh Token.
   */
  public function setRefreshToken($refresh_token) {
    $this->refresh_token = $refresh_token;
  }

  /**
   * Retrieve Logon URI.
   *
   * @since 1.0.0
   * @access public
   *
   * @return string Logon URI.
   */
  public function getLogonUri() {
    return $this->getLogonEndpoint().'?client_id='.$this->getClientId().'&client_secret='.$this->getClientSecret().'&response_type=code&redirect_uri='.$this->getCallbackUri();
  }

  /**
   * Execute Logon.
   *
   * @since 1.0.0
   * @access public
   */
  public function logon() {
    $url = $this->getLogonUri();
    header('Location: '.$url);
  }

  /**
   * Execute Logon with password.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $user Username.
   * @param string $pass Password.
   * @return string Access Token.
   */
  public function logonWithPassword($user, $pass) {
    return $this->retrieveAccessToken($user, $pass);
  }

  /**
   * Retrieve Access Token with Authorization Code.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $code Authorization Code.
   * @return string Access Token.
   */
  public function retrieveAccessTokenWithCode($code) {
    $url = $this->getTokenEndpoint().'?code='.$code.'&grant_type=authorization_code';

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    $result = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    if ($code != 200 && $code != 302)
      throw new MailUpException($code, 'Authorization error');

    $result = json_decode($result);

    $this->access_token = $result->access_token;
    $this->refresh_token = $result->refresh_token;

    $this->saveToken();

    return $this->access_token;
  }

  /**
   * Retrieve Access Token.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $user Username.
   * @param string $pass Password.
   * @return string Access Token.
   */
  public function retrieveAccessToken($user, $pass) {
    $url = $this->getTokenEndpoint();

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_POST, 1);

    $body = 'grant_type=password&username='.$user.'&password='.$pass.'&client_id='.$this->getClientId().'&client_secret='.$this->getClientSecret();

    $headers = array(
      'Content-length' => strlen($body),
      'Accept' => 'application/json',
      'Authorization' => 'Basic '.base64_encode($this->getClientId.':'.$this->getClientSecret())
    );

    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $body);

    $result = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    if ($code != 200 && $code != 302)
      throw new MailUpException($code, 'Authorization error');

    $result = json_decode($result);

    $this->access_token = $result->access_token;
    $this->refresh_token = $result->refresh_token;

    $this->saveToken();

    return $this->access_token;
  }

  /**
   * Refresh Access Token.

   * @since 1.0.0
   * @access public
   *
   * @return string Access Token.
   */
  public function refreshAccessToken() {
    $url = $this->getTokenEndpoint();
    $body = 'client_id='.$this->getClientId().'&client_secret='.$this->getClientSecret().'&refresh_token='.$this->getRefreshToken().'&grant_type=refresh_token';

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded', 'Content-length: '.strlen($body)));
    $result = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    if ($code != 200 && $code != 302)
      throw new MailUpException($code, 'Authorization error');

    $result = json_decode($result);

    $this->access_token = $result->access_token;
    $this->refresh_token = $result->refresh_token;

    $this->saveToken();

    return $this->access_token;
  }

  /**
   * Execute a call against MailUp API.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $url Endpoint URL.
   * @param string $verb HTTP verb.
   * @param string $body Optional. Request body. Default empty.
   * @param string $content_type Optional. Request content type. Default 'JSON'.
   * Accepts 'XML', 'JSON'.
   * @param bool $refresh Optional. Whether to refresh Access Token. Default true.
   * @return mixed Request result.
   */
  public function callMethod($url, $verb, $body = '', $content_type = 'JSON', $refresh = true) {
    $tmp = null;
    $c_type = ($content_type == 'XML' ? 'application/xml' : 'application/json');

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

    if ($verb == 'POST') {
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-type: '.$c_type,
        'Content-length: '.strlen($body),
        'Accept: '.$c_type,
        'Authorization: Bearer '.$this->access_token
      ));
    } elseif ($verb == 'PUT') {
      curl_setopt($curl, CURLOPT_PUT, true);
      $tmp = tmpfile();
      fwrite($tmp, $body);
      fseek($tmp, 0);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-type: '.$c_type,
        'Content-length: '.strlen($body),
        'Accept: '.$c_type,
        'Authorization: Bearer '.$this->access_token
      ));
      curl_setopt($curl, CURLOPT_INFILE, $tmp);
      curl_setopt($curl, CURLOPT_INFILESIZE, strlen($body));
    } elseif ($verb == DELETE) {
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-type: '.$c_type,
        'Content-length: 0',
        'Accept: '.$c_type,
        'Authorization: Bearer '.$this->access_token
      ));
    } else {
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-type: '.$c_type,
        'Content-length: 0',
        'Accept: '.$c_type,
        'Authorization: Bearer '.$this->access_token
      ));
    }

    $result = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($tmp != null)
      fclose($tmp);

    if ($code == 401 && $refresh) {
      $this->refreshAccessToken();
      return $this->callMethod($url, $verb, $body, $content_type, false);
    } elseif ($code == 401 && !$refresh) {
      throw new MailUpException($code, 'Authorization error');
    } elseif ($code != 200 && $code != 302) {
      throw new MailUpException($code, 'Unknown error');
    }

    return $result;
  }

  /**
   * Load Token from cookies.
   *
   * @since 1.0.0
   * @access public
   */
  public function loadToken() {
    foreach (array('access_token', 'refresh_token') as $token) {
      if (isset($_COOKIE[$token]))
        $this->$token = $_COOKIE[$token];
    }
  }

  /**
   * Save Token as cookie.
   *
   * @since 1.0.0
   * @access public
   */
  public function saveToken() {
    foreach (array('access_token', 'refresh_token') as $token) {
      setcookie($token, $this->$token, time()+60*60*24*30);
    }
  }
}
