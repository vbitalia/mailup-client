<?php
/**
 * MailUp REST API Exception.
 *
 * @package VB\MailUp
 * @copyright 2015 VB Italia Srl
 * @since 1.0.0
 */

namespace VB\MailUp;

/**
 * MailUp REST API Exception.
 *
 * @package VB\MailUp
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 1.0.0
 * @version 1.0.0
 */
class Exception extends \Exception {
  /**
   * HTTP Status Code.
   *
   * @since 1.0.0
   * @access private
   * @var int
   */
  private $status_code;

  /**
   * Setup Exception.
   *
   * @since 1.0.0
   * @access public
   *
   * @param int $status_code HTTP Status Code.
   * @param string $message Exception Message.
   */
  public function __construct($status_code, $message) {
    parent::__construct($message);
    $this->status_code = $status_code;
  }

  /**
   * Retrieve HTTP Status Code.
   *
   * @since 1.0.0
   * @access public
   *
   * @return int
   */
  public function getStatusCode() {
    return $this->status_code;
  }

  /**
   * Set HTTP Status Code.
   *
   * @since 1.0.0
   * @access public
   *
   * @param int $status_code HTTP Status Code.
   */
  public function setStatusCode($status_code) {
    $this->status_code = $status_code;
  }
}
